
# The Ideas and Goals of this example project:

This example project leverages a simple "hello world" run in parallel and making use of Fargate as a compute Resource. Further it shows the capability that fully managed infrastructure provides in order to have zero infrastructure efforts and maintenance. In concrete we are showing how to leverage AWS Fargate alongside another K8s Server Pool (GPU Enabled) in order to run AI workloads efficienly.

- Minimum Maintenance effort - Completely AWS Managed (even kube system) + use of karpenter autoscaler
- Cost efficiency and rapid(unlimited) scalability on demand leveraging AWS Fargate
- Configuration of Fargate profiles for specific namespaces
- Installation of GitLab Runner Helm Chart
- Leverage different GitLab Runner Managers and Node-Pools for different needs (EG. AI Workloads)


## Getting started

To get started you will need a Fargate backed EKS Cluster. You can achieve this by leveraging Terraform as described in the blog below or creating it manually.

https://about.gitlab.com/blog/2023/05/24/eks-fargate-runner/
https://gitlab.com/guided-explorations/aws/eks-runner-configs/gitlab-runner-eks-fargate/-/blob/main/README.md

